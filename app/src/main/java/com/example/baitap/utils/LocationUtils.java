package com.example.baitap.utils;

import android.util.Log;

import com.example.baitap.adapter.LoCationAdapter;
import com.example.baitap.api.ApiService;
import com.example.baitap.model.LocationItem;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingDeque;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class LocationUtils {

    public static Observable<ArrayList<LocationItem>> getLocationItem()
    {
        String base_URL="https://www.metaweather.com/";
        Retrofit retrofit=new Retrofit.Builder().baseUrl(base_URL)
                .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create()).build();
        ApiService api=retrofit.create(ApiService.class);
        return api.getDataLocation("san");
    }


}
