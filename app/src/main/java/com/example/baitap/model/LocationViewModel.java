package com.example.baitap.model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.baitap.utils.LocationUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observer;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class LocationViewModel extends ViewModel {
    private MutableLiveData<ArrayList<LocationItem>> mLisUserLiveData;
    private ArrayList<LocationItem> mListLocation;

    public LocationViewModel()
    {
        mLisUserLiveData=new MutableLiveData<>();
        initData();
    }

    private void initData() {
        mListLocation=new ArrayList<>();
        mListLocation.add(new LocationItem("12","2","3","4"));
        LocationUtils.getLocationItem()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<List<LocationItem>>() {
                    @Override
                    public void onSubscribe(@NonNull Disposable d) {

                    }

                    @Override
                    public void onNext(@NonNull List<LocationItem> locationItems) {
                        mListLocation.addAll(locationItems);
                        mLisUserLiveData.setValue(mListLocation);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });

//        LocationUtils.getLocationItem().
//        LocationUtils.getLocationItem(callBack);
//        Observable.just()
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe();


    }
    public MutableLiveData<ArrayList<LocationItem>> getListLocation()
    {
        return mLisUserLiveData;
    }
}
