package com.example.baitap.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.baitap.R;
import com.example.baitap.model.LocationItem;

import java.util.ArrayList;

public class LoCationAdapter extends RecyclerView.Adapter<LoCationAdapter.LocationHolder> {

    private ArrayList<LocationItem> loctionList;
    private OnclickListerner mListerner;


    public LoCationAdapter(ArrayList<LocationItem> loctionList) {
        this.loctionList = loctionList;
    }

    public void setOnItemclickListerner(OnclickListerner listerner)
    {
        mListerner=listerner;

    }
    public interface OnclickListerner
    {
        void itemclick(int position);
    }
    @NonNull
    @Override
    public LocationHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.location_item,parent,false);
       LocationHolder locationHolder=new LocationHolder(v,mListerner);
        return new LocationHolder(v,mListerner);
    }

    @Override
    public void onBindViewHolder(@NonNull LocationHolder holder, int position) {
        LocationItem locationItem=loctionList.get(position);
        if(locationItem==null)
        {
            return;
        }
        holder.txtTitle.setText(locationItem.getTitle());
        holder.txtLocationType.setText(locationItem.getLocation_type());
        holder.txtWoeid.setText(locationItem.getWoeid());
        holder.txtlattlong.setText(locationItem.getLatt_long());
    }

    @Override
    public int getItemCount() {
        if(loctionList!=null)
        {
            return loctionList.size();
        }
        else
            return 0;
    }

    public class LocationHolder extends RecyclerView.ViewHolder {
        public TextView txtTitle;
        public TextView txtLocationType;
        public TextView txtWoeid;
        public TextView txtlattlong;
        public LocationHolder(@NonNull View itemView,OnclickListerner listerner) {
            super(itemView);
            txtTitle=itemView.findViewById(R.id.txtTitle);
            txtLocationType=itemView.findViewById(R.id.txtLocationType);
            txtlattlong=itemView.findViewById(R.id.txtLattlong);
            txtWoeid=itemView.findViewById(R.id.txtWoied);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(mListerner!=null)
                    {
                        int position=getAdapterPosition();
                        if(position!=RecyclerView.NO_POSITION)
                            listerner.itemclick(position);
                    }
                }
            });
        }
    }
    }

