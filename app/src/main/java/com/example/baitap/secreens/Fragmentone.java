package com.example.baitap.secreens;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.baitap.R;
import com.example.baitap.adapter.LoCationAdapter;
import com.example.baitap.model.LocationItem;
import com.example.baitap.model.LocationViewModel;

import java.util.ArrayList;


public class Fragmentone extends Fragment {

    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager=new LinearLayoutManager(getActivity());
    private LoCationAdapter loCationAdapter;
    private LocationViewModel locationViewModel;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

              return inflater.inflate(R.layout.fragment_fragmentone, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        recyclerView=view.findViewById(R.id.rcv_location);
        recyclerView.setLayoutManager(linearLayoutManager);
       locationViewModel =new ViewModelProvider(getActivity()).get(LocationViewModel.class);
       locationViewModel.getListLocation().observe(getActivity(), new Observer<ArrayList<LocationItem>>() {
           @Override
           public void onChanged(ArrayList<LocationItem> locationItems) {
               Log.d("list", locationItems.size() + "");
               loCationAdapter=new LoCationAdapter(locationItems);
               loCationAdapter.notifyDataSetChanged();
               recyclerView.setAdapter(loCationAdapter);
               loCationAdapter.notifyDataSetChanged();
           }
       });
        super.onViewCreated(view, savedInstanceState);
    }
}