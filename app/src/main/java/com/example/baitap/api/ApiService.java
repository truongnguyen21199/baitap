package com.example.baitap.api;

import com.example.baitap.model.LocationItem;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.rxjava3.core.Observable;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiService {
    //https://www.metaweather.com/api/location/search/?query=san
    String base_URL="https://www.metaweather.com/";

    @GET("api/location/search/")
    Observable<ArrayList<LocationItem>> getDataLocation(@Query("query") String query);
//
//    @GET("api/location/search/")
//    Observable<ArrayList<LocationItem>> getDataLocation1(@Query("query") String query);

}
